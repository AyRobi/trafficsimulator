# TrafficSimulator
TrafficSimulator is a program able to simulate, at a microscopic level, the road traffic in multiple kinds of road junctions. 

This project has been carried out as part of our studies. The goal was to develop an application with the SARL language making in particular use of the agent-oriented simulation concepts learned during the semester.

![Image Imgur](https://i.imgur.com/4eGNt9e.png)

## Technologies
The main technologies used in this project are:
* [SARL language](http://www.sarl.io/) 0.10, a Java-based agent-oriented programming language.
* SARL Runtime Environment (SRE) 0.10, used to control the lifecyle of agents.
* JavaFX 8

## Getting started
### Prerequisites
* Windows 10 is recommended, the application has not been tested on other platforms.
* Java Runtime Environment 8

### Running
An archive containing the compiled application can be found on the [Releases](https://gitlab.com/AyRobi/trafficsimulator/-/releases) page. After having extracted all the archive content, the application can be launched by running `traffic_simulator.jar`.
```sh
java -jar traffic_simulator.jar
```

The `network` folder contains several simple road networks that can be directly loaded from the application.

## Simulator architecture
The simulator is mainly divided in three components, the code of which is located in its own package:

* **Data model:** Contains all the classes necessary to the representation of the simulated environment and the loading of the latter from a JSON file.
* **Engine:** The simulator core, based on the SRE and responsible for executing the simulation.
* **User interface:** The graphical interface, using the JavaFX library, enabling the configuration of the simulation and displaying the state of the simulation in real-time.

Further information on the simulator archicture can be found in the `doc` directory.

## Authors
* ALTENBACH Thomas - @taltenba
* ROBITAILLE Aymeric - @AyRobi
