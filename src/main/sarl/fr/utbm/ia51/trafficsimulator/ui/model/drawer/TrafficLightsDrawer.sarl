package fr.utbm.ia51.trafficsimulator.ui.model.drawer

import fr.utbm.ia51.trafficsimulator.ui.model.SimulationColors.ColorType
import fr.utbm.ia51.trafficsimulator.ui.model.SegmentedTrafficLight
import java.util.Collection
import fr.utbm.ia51.trafficsimulator.model.TrafficLight.State

/** 
 * Draw traffic lights
 * Warning : the given collection should be a set or insure uniqueness of its members, otherwise unexpected behavior could happen
 * @author Thomas ALTENBACH
 * @author Aymeric ROBITAILLE
 * 
 */
class TrafficLightsDrawer extends Drawer<Collection<SegmentedTrafficLight>> {
	static val SIGN_LINE_WIDTH = 0.3f

	var signLineWidth = SIGN_LINE_WIDTH.transformedWidth
	
	def internalDraw() {
		signLineWidth = SIGN_LINE_WIDTH.transformedWidth

		gc => [
			stroke = colors.get(ColorType.RED_LIGHT)
			lineWidth = signLineWidth
			lineDashes = 0
		]
		
		for (segmentedTrafficLight : value) {
			selectColor(segmentedTrafficLight.state)
			for (segment : segmentedTrafficLight.segments) {
				drawSegment(segment.offset(SIGN_LINE_WIDTH / 2), RoadMapDrawer::ROAD_LINE_NO_CROSS_OFFSET)
			}
		}
	}
	
	def internalClear() {
		gc.clearRect(0, 0, width, height)
	}
	
	private def selectColor(state : State) {
		var color : ColorType
		switch(state) {
			case State.RED:
				color = ColorType.RED_LIGHT
			case State.YELLOW:
				color = ColorType.YELLOW_LIGHT
			case State.GREEN:
				color = ColorType.GREEN_LIGHT
			default:
				color = ColorType.LINE
		}
		
		gc.stroke = colors.get(color)
	}
}
