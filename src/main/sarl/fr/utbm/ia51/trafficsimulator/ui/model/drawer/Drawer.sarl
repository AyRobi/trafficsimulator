package fr.utbm.ia51.trafficsimulator.ui.model.drawer

import javafx.scene.canvas.GraphicsContext
import org.eclipse.xtend.lib.annotations.Accessors
import fr.utbm.ia51.trafficsimulator.ui.model.SimulationColors
import fr.utbm.ia51.trafficsimulator.model.RoadMap
import java.util.Map
import fr.utbm.ia51.trafficsimulator.ui.model.SimulationColors.ColorType
import javafx.scene.paint.Color
import javafx.scene.canvas.Canvas
import javafx.beans.property.DoubleProperty
import javafx.beans.property.SimpleDoubleProperty
import fr.utbm.ia51.trafficsimulator.model.Rect
import fr.utbm.ia51.trafficsimulator.model.Vector3
import fr.utbm.ia51.trafficsimulator.model.Polyline
import javafx.scene.transform.Affine
import javafx.scene.transform.Rotate
import fr.utbm.ia51.trafficsimulator.model.Segment
import static extension fr.utbm.ia51.trafficsimulator.model.Vector3.*
import javax.annotation.Nullable

/** 
 * Represents a Drawer component
 * 
 * @author Thomas ALTENBACH
 * @author Aymeric ROBITAILLE
 * 
 */
abstract class Drawer<T> {
	static val VECTOR_ARROW_FACTOR = 1 / 6f
	static val EXTRA_OFFSET = 0.4f

	var simBox : Rect
	var translateVector : Vector3
	var scaleVector : Vector3
	var completeScaleVector : Vector3

	protected var colors : Map<ColorType, Color>
	protected val gc : GraphicsContext

	@Accessors(PROTECTED_GETTER) var curSimulationTime : long
	
	@Accessors @Nullable var value : T
	@Accessors(PUBLIC_GETTER) val heightProperty : DoubleProperty
	@Accessors(PUBLIC_GETTER) val widthProperty : DoubleProperty

	protected new(canvas : Canvas, colors : SimulationColors) {
		this(canvas.height, canvas.width, colors, canvas.graphicsContext2D)
	}

	protected new(height : double, width : double, colors : SimulationColors, gc : GraphicsContext) {
		this.gc = gc
		this.heightProperty = new SimpleDoubleProperty(this, "height", height)
		this.widthProperty = new SimpleDoubleProperty(this, "width", width)
		this.completeScaleVector = new Vector3(1, 1)
		this.scaleVector = new Vector3(1, 1)
		this.translateVector = new Vector3(0, 0)
		this.colors = colors.colorMap

		this.heightProperty.addListener [ o, oldH, newH |
			var scaleY = completeScaleVector.y as double
			scaleY *= newH.toDouble / oldH.toDouble
			completeScaleVector = new Vector3(completeScaleVector.x, scaleY)

			computeUniformScale()
		]

		this.widthProperty.addListener [ o, oldW, newW |
			var scaleX = completeScaleVector.x as double
			scaleX *= newW.toDouble / oldW.toDouble
			completeScaleVector = new Vector3(scaleX, completeScaleVector.y)

			computeUniformScale()
		]

		clear()
	}

	@Pure
	def final getWidth() : double {
		return widthProperty.get()
	}

	def final setWidth(width : double) {
		widthProperty.set(width)
	}

	@Pure
	def final getHeight() : double {
		return heightProperty.get()
	}

	def final setHeight(height : double) {
		heightProperty.set(height)
	}

	def final setTransformFromRoadMap(roadMap : RoadMap) {
		if (roadMap === null)
			return

		var xMin : float = Float.MAX_VALUE
		var yMin : float = Float.MAX_VALUE
		var xMax : float = 0
		var yMax : float = 0

		var box : Rect

		for (road : roadMap.roads) {
			val offset = road.width / 2 + EXTRA_OFFSET
			box = road.shape.computeBoundingBox2D()
			box = new Rect(
				box.x - offset,
				box.y - offset,
				box.width + 2 * offset,
				box.height + 2 * offset
			)

			xMin = Math::min(box.x, xMin)
			yMin = Math::min(box.y, yMin)
			xMax = Math::max(box.x + box.width, xMax)
			yMax = Math::max(box.y + box.height, yMax)
		}

		for (junction : roadMap.junctions) {
			box = junction.shape.computeBoundingBox2D()

			xMin = Math::min(box.x, xMin)
			yMin = Math::min(box.y, yMin)
			xMax = Math::max(box.x + box.width, xMax)
			yMax = Math::max(box.y + box.height, yMax)
		}

		val xScale = xMax - xMin != 0 ? width / (xMax - xMin) : 1
		val yScale = yMin - yMax != 0 ? height / (yMin - yMax) : 1

		this.simBox = new Rect(
			xMin,
			yMin,
			xMax - xMin,
			yMax - yMin
		)

		this.completeScaleVector = new Vector3(xScale, yScale)
		computeUniformScale()
	}

	private def computeUniformScale() {
		val minScale = Math::min(completeScaleVector.x, -completeScaleVector.y)
		scaleVector = new Vector3(minScale, -minScale)
		
		if (simBox === null) {
			this.translateVector = new Vector3(0, 0)
			return
		}
		
		this.translateVector = new Vector3(
			-simBox.x + (width / scaleVector.x - simBox.width) / 2,
			-(simBox.y + simBox.height) - (height / -scaleVector.y - simBox.height) / 2
		)
	}

	def final copyTransform(drawer : Drawer<?>) {
		this.simBox = drawer.simBox
		this.translateVector = drawer.translateVector
		this.completeScaleVector = drawer.completeScaleVector
		this.scaleVector = drawer.scaleVector
	}

	def final setColors(colors : SimulationColors) {
		this.colors = colors.colorMap
	}

	def final draw() {
		internalClear()

		if (value !== null)
			internalDraw()
	}

	def final draw(value : T, curSimulationTime : long) {
		this.value = value
		this.curSimulationTime = curSimulationTime
		draw()
	}

	def final clear() {
		value = null
		internalClear()
	}
	
	def final toMetricCoordinates(canvasCoordinates : Vector3) : Vector3 {
		return unTransform(canvasCoordinates)
	}

	protected abstract def internalDraw()

	protected abstract def internalClear()

	protected def transform(vector : Vector3) : Vector3 {
		return vector.transform(translateVector, scaleVector)
	}
	
	protected final def unTransform(v : Vector3) : Vector3 {
		return new Vector3(
			v.x / scaleVector.x - translateVector.x,
			v.y / scaleVector.y - translateVector.y
		)
	}

	/** 
	 * Draws a shape defined by an upper line and a lower line
	 * 
	 * @param u upper PolyLine
	 * @param l lower PolyLine
	 */
	protected final def drawShape(u : Polyline, l : Polyline) {
		val reversedL = l.reverse()
		var iter = u.iterator

		if (!iter.hasNext)
			return;

		val firstVertex = iter.next.transform
		gc.beginPath()
		gc.moveTo(firstVertex.x, firstVertex.y)

		while (iter.hasNext) {
			val vertex = iter.next.transform
			gc.lineTo(vertex.x, vertex.y)
		}

		for (vertex : reversedL) {
			val transformedVertex = vertex.transform
			gc.lineTo(transformedVertex.x, transformedVertex.y)
		}

		gc.closePath()
		gc.fill()
	}

	/** 
	 * Draws a closed shape defined by a PolyLine
	 * 
	 * @param shape The shape to be drawn
	 */
	protected final def drawShape(shape : Polyline) {
		val iter = shape.iterator

		if (!iter.hasNext)
			return;

		val firstVertex = iter.next.transform
		gc.beginPath()
		gc.moveTo(firstVertex.x, firstVertex.y)

		while (iter.hasNext) {
			val vertex = iter.next.transform
			gc.lineTo(vertex.x, vertex.y)
		}

		gc.closePath()
		gc.fill()
	}

	/** 
	 * Draws a simple PolyLine
	 * 
	 * @param p The PolyLine to draw
	 */
	protected final def drawPolyLine(p : Polyline) {
		val iter = p.iterator

		if (!iter.hasNext)
			return;

		val firstVertex = iter.next.transform
		gc.beginPath()
		gc.moveTo(firstVertex.x, firstVertex.y)

		while (iter.hasNext) {
			val vertex = iter.next.transform
			gc.lineTo(vertex.x, vertex.y)
		}

		gc.stroke()
	}

	/** 
	 * Draws a segment
	 * 
	 * @param segment The segment to be drawn
	 */
	protected final def drawSegment(segment : Segment, innerOffset : float) {
		val offseted = segment.shrink(innerOffset)
		val start = offseted.start.transform
		val end = offseted.end.transform

		gc.strokeLine(
			start.x,
			start.y,
			end.x,
			end.y
		)
	}

	/** 
	 * Draws a vector (arrow representation)
	 * 
	 * @param start The start point from which the vector will be drawn
	 * @param vect The vector to draw
	 */
	protected final def drawVector(start : Vector3, vect : Vector3) {
		val startP = start.transform
		val endP = (start + vect).transform

		val arrowMidP = new Vector3(
			(startP.x - endP.x) * VECTOR_ARROW_FACTOR + endP.x,
			(startP.y - endP.y) * VECTOR_ARROW_FACTOR + endP.y
		)
		val trans = (endP - arrowMidP).norm * Math::sqrt(2) / 2

		val arrowLeft = arrowMidP - new Vector3(trans, trans)
		val arrowRight = arrowMidP + new Vector3(-trans, trans)

		gc.strokeLine(
			startP.x,
			startP.y,
			endP.x,
			endP.y
		)

		gc.strokeLine(
			endP.x,
			endP.y,
			arrowRight.x,
			arrowRight.y
		)
		gc.strokeLine(
			endP.x,
			endP.y,
			arrowLeft.x,
			arrowLeft.y
		)
	}

	/** 
	 * Draws a plain circle
	 * 
	 * @param c The center of the circle
	 * @param radius The circle radius
	 */
	protected final def drawCircle(c : Vector3, radius : float) {
		val c1 = c.transform
		val scaledRadius = radius * scaleVector

		gc.fillOval(
			c1.x - scaledRadius.x,
			c1.y + scaledRadius.y,
			scaledRadius.x * 2,
			-scaledRadius.y * 2
		)
	}

	/** 
	 * Draws a rectangle
	 * 
	 * @param rect The rectangle to draw
	 */
	protected final def drawRect(rect : Rect) {
		val corner = new Vector3(rect.x, rect.y + rect.height).transform
		gc.strokeRect(
			corner.x,
			corner.y,
			rect.width * scaleVector.x,
			-rect.height * scaleVector.y
		)
	}

	/** 
	 * Fills a rectangle
	 * 
	 * @param rect The rectangle to fill
	 */
	protected final def fillRect(rect : Rect) {
		val corner = new Vector3(rect.x, rect.y + rect.height).transform
		gc.fillRect(
			corner.x,
			corner.y,
			rect.width * scaleVector.x,
			-rect.height * scaleVector.y
		)
	}

	/** 
	 * Creates a rotation transform
	 * 
	 * @param r The rotation angle in degree between axis x and the rotation axis
	 */
	protected final def rotateByOrigin(r : float) {
		val o = new Vector3(0, 0)
		rotateByOrigin(o, r)
	}

	/** 
	 * Creates a rotation transform
	 * 
	 * @param p The rotation origin
	 * @param r The rotation angle in degree between axis x and the rotation axis
	 */
	protected final def rotateByOrigin(o : Vector3, r : float) {
		val o2 = o.transform
		gc.transform = new Affine(
			new Rotate(r, o2.x, o2.y)
		)
	}

	/** 
	 * Converts a meter like width into a scaled width.
	 * 
	 * @param width The meter like width
	 * @return The scaled width
	 */
	protected final def transformedWidth(width : float) : float {
		return width * scaleVector.x
	}
}
