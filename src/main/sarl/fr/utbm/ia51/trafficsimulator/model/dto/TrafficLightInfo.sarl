package fr.utbm.ia51.trafficsimulator.model.dto

import fr.utbm.ia51.trafficsimulator.model.TrafficLight
import org.eclipse.xtend.lib.annotations.Data

/** 
 * Traffic light DTO.
 * @author Thomas ALTENBACH
 * @author Aymeric ROBITAILLE
 */
@Data
final class TrafficLightInfo {
	val id : String
	val phases : PhaseInfo[]
	
	/* Required by Gson */
	private new() {
		id = null
		phases = null
	}
	
	new(id : String, phases : PhaseInfo[]) {
		this.id = id
		this.phases = phases
	}
	
	@Data
	static final class PhaseInfo {
		val state : TrafficLight.State
		val duration : int
		
		/* Required by Gson */
		private new() {
			state = null
			duration = 0
		}
		
		new(state : TrafficLight.State, duration : int) {
			this.state = state
			this.duration = duration
		}
	}
}
