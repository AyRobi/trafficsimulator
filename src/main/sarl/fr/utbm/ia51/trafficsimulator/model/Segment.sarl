package fr.utbm.ia51.trafficsimulator.model

import javax.annotation.concurrent.Immutable
import org.eclipse.xtend.lib.annotations.Data

import static extension fr.utbm.ia51.trafficsimulator.model.Vector3.*

/** 
 * Represents a segment between two points.
 * @author Thomas ALTENBACH
 * @author Aymeric ROBITAILLE
 */
@Data
@Immutable
final class Segment {
	/**
	 * The segment start point.
	 */
	val start : Vector3
	
	/**
	 * The segment end point.
	 */
	val end : Vector3
	
	@Pure
	def getDirection() : Vector3 {
		return end - start
	}

	@Pure
	def getReverseDirection() : Vector3 {
		return start - end
	}
	
	/**
	 * Creates a new segment that is the result of the translation of this segment.
	 * 
	 * @param v The translation vector.
	 * @param k The translation factor.
	 * @return The translation result.
	 */
	@Pure
	def translate(v : Vector3, k : float = 1.0f) : Segment {
		return new Segment(start.translate(v, k), end.translate(v, k))
	}
	
	/**
	 * Creates a new segment that is the result of the translation of this segment along this segment normal.
	 * 
	 * A positive offset translates the segment to the left, in the segment reference frame, oriented from the start
	 * point to the end point, and a negative offset translates the segment to the right.
	 * 
	 * @param value The offset value.
	 * @return The translation result.
	 */
	@Pure
	def offset(value : float) : Segment {
		var normal = Vector3.orthoNormalize2D(start, end)
		return translate(normal, value)
	}

	/** 
	 * Creates a new segment that is a result of a reduction of this segment size, the reduction is done on each side of
	 * the segment
	 * 
	 * A positive offset decrease the segment size
	 * 
	 * @param value The offset value
	 * @return The reduction result
	 */
	@Pure
	def shrink(value : float) : Segment {
		val director = end - start
		return new Segment(start.translate(director, value), end.translate(director, -value))
	}
	
	// This function does not handle overlapping segments
	@Pure
	def intersect2D(other : Segment) : Vector3 {
		var intersectPoint = intersectOffsets2D(other)
		
		if (intersectPoint === null)
			return null
		
		return Vector3.lerp(start, end, intersectPoint.myNormalizedOffset)
	}

	// This function does not handle overlapping segments
	@Pure
	def intersectOffsets2D(other : Segment) : IntersectionPoint {
		var r = end - start
		var s = other.end - other.start

		var qp = other.start - start

		var rCrossS = r.cross2D(s)

		// The two segments are parallel
		if (Math.abs(rCrossS) < Vector3.FLOAT_EPSILON)
			return null

		var t = qp.cross2D(s) / rCrossS
		var u = qp.cross2D(r) / rCrossS

		if (t < 0.0f || t > 1.0f + Vector3.FLOAT_EPSILON || u < 0.0f || u > 1.0f + Vector3.FLOAT_EPSILON)
			return null

		return new IntersectionPoint(Math.min(t, 1.0f), Math.min(u, 1.0f))
	}

	/** 
	 * Compute the slope of the line defined by this segment.
	 * 
	 * If the line is vertical, Float.POSITIVE_INFINITY is returned.
	 * 
	 * @return The computed slope.
	 */
	@Pure
	def getLineSlope2D() : float {
		var dx = end.x - start.x
		
		if (Math.abs(dx) < Vector3.FLOAT_EPSILON)
			return Float.POSITIVE_INFINITY
		
		return (end.y - start.y) / (end.x - start.x)
	}
	
	@Pure
	def private computeLineIntercept2D(slope : float) : float {
		return start.y - slope * start.x
	}
	
	/**
	 * Computes the intersection between the line defined by this segment and the one defined by another given segment.
	 * 
	 * @param other The other segment.
	 * @return The intersection between the two lines if any, null otherwise.
	 */
	@Pure
	def lineIntersect2D(other : Segment) : Vector3 {
		var thisSlope = this.lineSlope2D
		var otherSlope = other.lineSlope2D
		
		// First statement requirement in case both slope are +inf since +inf - (+inf) == Nan
		if (thisSlope == otherSlope || Math.abs(thisSlope - otherSlope) < Vector3.FLOAT_EPSILON)
			return null

		if (thisSlope == Float.POSITIVE_INFINITY)
			return new Vector3(start.x, otherSlope * start.x + other.computeLineIntercept2D(otherSlope))
		
		if (otherSlope == Float.POSITIVE_INFINITY)
			return new Vector3(other.start.x, thisSlope * other.start.x + computeLineIntercept2D(thisSlope))
		
		var thisIntercept = this.computeLineIntercept2D(thisSlope)
		var otherIntercept = other.computeLineIntercept2D(otherSlope)
		
		var x = (otherIntercept - thisIntercept) / (thisSlope - otherSlope)
		var y = thisSlope * x + thisIntercept
		
		return new Vector3(x, y)
	}

	@Data
	@Immutable
	static final class IntersectionPoint {
		val myNormalizedOffset : float
		val otherNormalizedOffset : float
	}
}
