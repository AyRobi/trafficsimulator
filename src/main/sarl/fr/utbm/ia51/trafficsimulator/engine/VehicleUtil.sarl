package fr.utbm.ia51.trafficsimulator.engine

import fr.utbm.ia51.trafficsimulator.model.VehicleState

/**
 * Utility methods for vehicles.
 * 
 * @author Thomas ALTENBACH
 * @author Aymeric ROBITAILLE
 */
final class VehicleUtil {
	/**
	 * Minimum gap between two vehicles, in meters.
	 */
	public static val MIN_VEHICLE_GAP = 2.5f
	
	/**
	 * Gap between the front of the vehicle and the intersection line when the vehicle is stopped at the intersection.
	 */
	public static val INTERSECTION_STOP_GAP = 0.5f
	
	/**
	 * Safe time headway, in seconds.
	 */
	public static val SAFE_TIME_HEADWAY = 2.0f
	
	/**
	 * Comfortable deceleration,  in m/s².
	 */
	public static val COMFORTABLE_DECELERATION = 1.67f
	
	/**
	 * Sight distance, in meters.
	 */
	public static val SIGHT_DISTANCE = 300.0f
	
	/**
	 * Distance to the next junction, in meters, below which a vehicle is considered to be close to a junction.
	 */
	public static val JUNCTION_APPROACH_DISTANCE = 25.0f
	
	/**
	 * Minimum gap, in meters, between two vehicles when crossing each other.
	 */
	public static val MIN_CROSSING_GAP = 5.0f
	
	/**
	 * Minimal waiting time at a stop sign, in milliseconds.
	 */
	public static val MIN_STOP_DURATION = 3000f

	/**
	 * The value, in m/s, below which a car that is waiting at stop is considered to have stopped.
	 */
	public static val STOP_SPEED_THRESHOLD = 0.1f
	
	/** 
	 * The value, in m/s², below which a floating point number representing an acceleration is considered to be zero.
	 */
	public static val ACCELERATION_EPSILON = 0.01f

	/** 
	 * The value, in m/s, below which a floating point number representing a speed is considered to be zero.
	 * 
	 * This speed must be greater than or equal to the speed change during the lowest possible time quantum for a
	 * vehicle accelerating with the lowest non-zero acceleration, that is ACCELERATION_EPSILON.
	 */
	public static val SPEED_EPSILON = ACCELERATION_EPSILON / 1000.0f
	
	private new {
		// Empty
	}
	
	/**
	 * Returns the braking distance for a vehicle, given its speed.
	 * 
	 * @param speed The speed of the vehicle, in m/s.
	 * @return The braking distance of the vehicle.
	 */
	def static minGap(vehicleSpeed : float) : float {
		return Math.max(MIN_VEHICLE_GAP, vehicleSpeed * SAFE_TIME_HEADWAY);
	}

	def static computeSpeedChangeOffset(curState : VehicleState, newSpeed : float, accel : float) : float {
		var curSpeed = curState.speed
		var curOffset = curState.laneOffset
		var deltaSpeed = newSpeed - curSpeed
		
		if (Math.abs(deltaSpeed) < SPEED_EPSILON)
			return curOffset
		
		if (Math.abs(accel) < ACCELERATION_EPSILON)
			return Float.POSITIVE_INFINITY

		var speedChangeDuration = deltaSpeed / accel
		
		if (speedChangeDuration < 0.0f)
			return 0.0f
		
		return (accel * speedChangeDuration + curSpeed) * speedChangeDuration + curOffset
	}

	def static computeStoppingOffset(curState : VehicleState, brakingAccel : float) : float {
		return computeSpeedChangeOffset(curState, 0.0f, brakingAccel)
	}
}
